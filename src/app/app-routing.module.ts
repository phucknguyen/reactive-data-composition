import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const importDataComp = () => {
  return import('./data-comp/data-comp.module')
    .then(m => m.DataCompModule);
};

const routes: Routes = [
  { path: '', loadChildren: importDataComp }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
