import { Injectable } from '@angular/core';

import * as rx from 'rxjs';
import * as op from 'rxjs/operators';

import { Product } from './product';
import { ProductCategory } from './product-category';
import { Supplier } from './supplier';

import { DataCompContextService } from './data-comp-context.service';
import { DataCompControlOut } from './data-comp.control.io';


@Injectable({
  providedIn: 'root'
})
export class DataCompControl {

  private selectedProductId$ = new rx.Subject<number>();

  private productData$: rx.Observable<Product[]> =
    this.context.fetchProducts();

  private productCategoryData$: rx.Observable<ProductCategory[]> =
    this.productData$.pipe(
      op.switchMap(products => {
        return this.context.fetchProductCategories(products);
      })
    );

  private supplierData$: rx.Observable<Supplier[]> =
    this.productData$.pipe(
      op.switchMap(products => {
        return this.context.fetchSuppliers(products);
      })
    );

  readonly output$: rx.Observable<DataCompControlOut> = rx.merge(
    this.selectedProductId$.pipe(
      op.map<number, Partial<DataCompControlOut>>(
        selectedProductId => ({selectedProductId})
      )
    ),
    this.productData$.pipe(
      op.map<Product[], Partial<DataCompControlOut>>(
        productData => ({products: productData})
      )
    ),
    this.productCategoryData$.pipe(
      op.map<ProductCategory[], Partial<DataCompControlOut>>(
        categoryData => ({categories: categoryData})
      )
    ),
    this.supplierData$.pipe(
      op.map<Supplier[], Partial<DataCompControlOut>>(
        supplierData => ({suppliers: supplierData})
      )
    ),
  ).pipe(
    op.scan<Partial<DataCompControlOut>, DataCompControlOut>(
      (acc, val) => ({...acc, ...val}), new DataCompControlOut()
    )
  );

  constructor(
    private context: DataCompContextService
  ) { }

  selectProduct(productId: number) {
    this.selectedProductId$.next(productId);
  }

}

