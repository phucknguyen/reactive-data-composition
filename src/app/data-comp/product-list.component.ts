import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import * as rx from 'rxjs';
import * as op from 'rxjs/operators';

import { Product } from './product';
import { ProductCategory } from './product-category';

import { DataCompControl } from './data-comp.control';
import { DataCompControlOut } from './data-comp.control.io';

import { ProductListOut } from './product-list.io';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductListComponent implements OnInit {

  readonly output$: rx.Observable<ProductListOut> =
    this.control.output$.pipe(
      op.map<DataCompControlOut, ProductListOut>(state => {
        const products = state.products.map(product => {
          const id = product.id;
          const selected = (product.id === state.selectedProductId);
          const category = state.categories.find(
            category => (category.id === product.categoryId)
          );
          const nameAndCategory = (
            category
            ? `${product.productName} (${category.name})`
            : `${product.productName}`
          );
          return {
            id, selected, nameAndCategory
          };
        });
        return {
          title: 'Products',
          products,
        };
      })
    );

  constructor(private control: DataCompControl) { }

  ngOnInit(): void { }

  onSelected(selectedProductId: string) {
    const productId = Number(selectedProductId);
    if (!isNaN(productId)) {
      this.control.selectProduct(Number(productId));
    }
  }

}
