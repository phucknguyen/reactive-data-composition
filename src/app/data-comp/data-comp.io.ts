import { ProductListOut } from './product-list.io';
import { ProductDetailsOut } from './product-details.io';

export class DataCompOut {
  productList: ProductListOut;
  productDetails: ProductDetailsOut;
}
