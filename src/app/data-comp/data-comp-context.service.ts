import { Injectable } from '@angular/core';

import * as rx from 'rxjs';
import * as op from 'rxjs/operators';

import { Product } from './product';
import { ProductData } from './product-data';
import { ProductCategory } from './product-category';
import { ProductCategoryData } from './product-category-data';
import { Supplier } from './supplier';
import { SupplierData } from './supplier-data';

@Injectable({
  providedIn: 'root'
})
export class DataCompContextService {

  constructor() { }

  fetchProducts(): rx.Observable<Product[]> {
    return rx.of(ProductData.products);
  }

  fetchProductCategories(
    products: Product[]
  ): rx.Observable<ProductCategory[]> {
    return rx.of(ProductCategoryData.categories);
  }

  fetchSuppliers(
    products: Product[]
  ): rx.Observable<Supplier[]> {
    return rx.of(SupplierData.suppliers);
  }

}
