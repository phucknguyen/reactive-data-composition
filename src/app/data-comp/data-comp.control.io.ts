import { Product } from './product';
import { ProductCategory } from './product-category';
import { Supplier } from './supplier';


export class DataCompControlOut {
  selectedProductId?: number;
  products: Product[] = [];
  categories: ProductCategory[] = [];
  suppliers: Supplier[] = [];
}
