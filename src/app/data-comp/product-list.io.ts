export class ProductListOut {
  title: string;
  products: {
    id: number;
    selected: boolean;
    nameAndCategory: string;
  }[];
}
