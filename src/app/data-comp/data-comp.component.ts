import {
  Component, OnInit, ChangeDetectionStrategy
} from '@angular/core';

@Component({
  selector: 'app-data-comp',
  templateUrl: './data-comp.component.html',
  styleUrls: ['./data-comp.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataCompComponent implements OnInit {

  constructor() { }

  ngOnInit(): void { }

}
