import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import * as rx from 'rxjs';
import * as op from 'rxjs/operators';

import { Product } from './product';
import { ProductCategory } from './product-category';

import { DataCompControl } from './data-comp.control';
import { DataCompControlOut } from './data-comp.control.io';

import { ProductDetailsOut } from './product-details.io';


@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductDetailsComponent implements OnInit {

  readonly output$: rx.Observable<ProductDetailsOut> =
    this.control.output$.pipe(
      op.map<DataCompControlOut, ProductDetailsOut>(data => {
        const product: Product = data.products.find(
          product => (product.id === data.selectedProductId)
        );
        if (!product) { return null; }
        const title = `Product Details for ${product.productName}`;
        const category: ProductCategory = data.categories.find(
          category => (category.id === product.categoryId)
        );
        const categoryName = (category ? category.name : '');
        const suppliers = data.suppliers
          .filter(supplier => {
            return product.supplierIds.includes(supplier.id);
          })
          .map(supplier => {
            return {...supplier, cost: '$' + supplier.cost};
          });
        return {
          title,
          name: product.productName,
          code: product.productCode,
          description: product.description,
          price: '$' + product.price,
          categoryName,
          suppliers,
        };
      })
    );

  constructor(private control: DataCompControl) { }

  ngOnInit(): void { }

}
