export class ProductDetailsOut {
  title: string;
  name: string;
  code: string;
  description: string;
  price: string;
  categoryName: string;
  suppliers: {
    id: number;
    name: string;
    cost: string;
    minQuantity: number;
  }[];
}

