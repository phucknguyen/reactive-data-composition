import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DataCompComponent } from './data-comp.component';

const routes: Routes = [{ path: '', component: DataCompComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataCompRoutingModule { }
